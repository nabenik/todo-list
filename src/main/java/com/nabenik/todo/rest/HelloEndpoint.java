package com.nabenik.todo.rest;

import com.nabenik.todo.model.Item;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;

@RequestScoped
@Path("/hello")
@Produces("text/plain")
@Consumes("text/plain")
public class HelloEndpoint {
    
    
    @GET
    public Response doReverse(@QueryParam("name") final String name) {
        String reversedName = "NaS";
        if(!StringUtils.isEmpty(name)){
            reversedName = StringUtils.reverse(name);
        }
               
        return Response.ok(reversedName).build();
    }
}
