/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.todo.rest;

import com.nabenik.todo.util.TestConfig;
import java.io.File;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import static org.glassfish.internal.embedded.ScatteredArchive.Builder.type.war;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.ScopeType;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;

/**
 *
 * @author tuxtor
 */
@RunWith(Arquillian.class)
public class HelloEndpointIT {
    
    @Deployment
    public static WebArchive createDeployment() {
        
        WebArchive webArchive = ShrinkWrap
                .create(WebArchive.class, "hello-rest-test.war")
                .addClasses(RestApplication.class, HelloEndpoint.class)
                .addAsWebInfResource("test-beans.xml", "beans.xml");
        
        //Resolving files manually
        File[] files = Maven.resolver()
                .resolve("org.apache.commons:commons-lang3:3.7")
                .withTransitivity().asFile();

        if(files != null){

            webArchive.addAsLibraries(files);

        }
        
        System.out.println(webArchive.toString(true));
        
        return webArchive;
    }
    
    @Test
    @InSequence(1)
    public void testReverseString() {
        WebTarget target = ClientBuilder.newClient()
                .target(TestConfig.TEST_BASE_URL + "/hello-rest-test/rest/hello");
        
        
        String reversedString = target.queryParam("name", "victor").request("text/plain").get(String.class);

        assertEquals("rotciv", reversedString);
    }

    
}
